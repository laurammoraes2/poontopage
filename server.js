

const express = require('express');
const bodyParser = require('body-parser');

const expressLayouts = require('express-ejs-layouts')

const path = require('path');



const cookieParser = require('cookie-parser');

var favicon = require('serve-favicon');



  
  

// App
const app = express();
app.use(cookieParser())




app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(expressLayouts);
app.use(express.static(__dirname + '/public'));




                                
var port = process.env.PORT  || 21070;


app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')));



  




// Load routes
const indexRoutes = require('./routes/app');
app.use('/', indexRoutes);

app.listen(port, function () {
    console.log('Poonto listening on port %s', port);
});

module.exports = app;